module.exports = {
  '*.js': ['eslint --fix', 'prettier'],
  '*.md'(filenames) {
    return filenames.map((filename) => `remark ${filename} --output`);
  },
};

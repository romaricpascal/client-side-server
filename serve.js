/**
 * Little express server that mimicks how the app will be served
 * on Gitlab pages (ie. under the `/client-side-server` host)
 */

import { join } from 'node:path';
import express from 'express';
const app = express();
const port = 3000;

const router = new express.Router();
// Serve the docs under /docs
router.use('/docs', express.static(join(process.cwd(), 'docs')));
// Serve the files in `src`
router.use(express.static(join(process.cwd(), 'src')));
// Rewrite any other URL as `index.html`
router.use((req, res) => {
  res.sendFile(join(process.cwd(), 'src', 'index.html'));
});

// Use the router to serve anything under `/client-side-server`
app.use('/client-side-server', router);

// And redirect any other request to `/client-side-server/` for convenience
app.use((req, res) => {
  res.redirect('/client-side-server/');
});

app.listen(port, () => {
  console.log(`Server started at http://localhost:${port}`);
});

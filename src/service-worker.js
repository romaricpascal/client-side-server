/// <reference no-default-lib="true"/>
/// <reference lib="es2015" />
/// <reference lib="webworker" />

// An experimental service worker that handles all fetch requests, coming from
// the controlled pages, computing and rendering locally rather than reaching
// for a server.

// ## Intercepting browser requests
//
// Once the service worker is activated, requests from a page
// it controls will generate a [FetchEvent]. We can use it to
// intercept that request and send a custom respond, either of a cached asset or from the client side server

// [FetchEvent]: https://developer.mozilla.org/en-US/docs/Web/API/FetchEvent
self.addEventListener('fetch', handleFetch);

// To help process the response, we pre-extract two pieces of information from
// the fetch event, as they'll likely be used:
// - the method of the request
// - the URL of the request, as an actual [URL] object
//
// We also handle we also create a structure for collecting the response, so
// that we can easily pass the `body` and headers/statuses when constructing the
// actual [Response], or notify the handler to let the request carry on to the server

// [URL]: https://developer.mozilla.org/en-US/docs/Web/API/URL
// [Response]: https://developer.mozilla.org/en-US/docs/Web/API/Response
function handleFetch(event) {
  const url = new URL(event.request.url);
  const requestInfo = {
    method: event.request.method,
    url,
    request: event.request,
    event,
  };

  const responseInfo = respondTo(requestInfo);
  if (responseInfo && responseInfo !== FORWARD_TO_SERVER) {
    event.respondWith(createResponse(responseInfo));
  }
}

// Little subtlety here: `handleFetch` needs to call `event.respondWith` synchronously.
// Kind of makes sense as you'd want the browser to know as soon as possible that the request is being handled
// by the service worker.
// `event.respondWith` can receive a [Promise] and will wait for it to be resolved before responding,
// allowing the server to do asynchronous operations.
//
// [Promise]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise
async function createResponse(responseInfo) {
  const awaitedResponseInfo = await responseInfo;

  /*
   * Allow the handler do build a response themseleves,
   * for example using Response.redirect
   */
  if (awaitedResponseInfo instanceof Response) {
    return awaitedResponseInfo;
  }

  /**
   * @todo: Check if we can still forward to the server,
   * maybe passing the original request and using `fetch`?
   */
  return new Response(
    awaitedResponseInfo.body,
    awaitedResponseInfo.responseOptions,
  );
}

const FORWARD_TO_SERVER = Symbol();

// ## Routing
//
// Our server shouldn't send the same response for all URLs
// so needs some way to pick the right response to the request
// To keep things simple, it'll just go through an array of routes
// and the first to provide a response will stop the iteration
// Up to each route function to decide whether it needs to run or not
// and whether to return something or not
function respondTo(requestInfo) {
  for (var i = 0; i < ROUTES.length; i++) {
    /**
     * @todo Add more flexibility, for example passing the current response
     * to the next handler
     */
    const response = ROUTES[i](requestInfo);
    if (response) {
      return response;
    }
  }
}

// ## Route helpers
//
// Routes will share some logic to check if they need to execute or not:
//
// - matching the method
// - matching the path
// - content negociation
//
// It'll pay off to abstract each of them into their own helper.
//
// To help keep this matching logic separate from the logic for responding
// itself, we'll add a little helper to combine these functions themselves, in a
// similar fashion to Express' middleware.
//
// We'll create a "chain" of functions, with each step in the chain receiving a
// function to call the next one. Up to each step to decide whether to cary on
// or just return (or even modify what the next step returned)
//
// Each function will be in the form of `function(requestInfo, next)`
// and may return a ResponseInfo as/if they see fit. The `chain` helper
// will allow to combine them in calls that look like:
//
// ```
// chain(
//   method('GET'),
//   path('/somewhere'),
//   function(requestInfo) {
//     /*
//       Actually respond here
//       But only to GET requests
//       to '/somewhere'
//     */
//   }
// )
// ```
/**
 * Chains the steps provided so that each receive a final `next` function
 * allowing them to execute the next step if/when appropriate.
 *
 * Each step can pass new arguments when executing the next step.
 * The created function will return the result of the last step that was run,
 * or any non-function value that gets returned.
 *
 * @param  {...Function|any} steps
 * @returns {any}
 */
function chain(...steps) {
  /* 
    That's the function combining the steps,
    it'll receive the initial arguments
    and start the chain
  */
  return function (...args) {
    return createStepRunner(0)();

    /**
     * Creates a function that'll execute the step at the given index (or return it if it's  not a function)
     *
     * @param {number} index - The index of the step to execute
     * @returns {any}
     */
    function createStepRunner(index) {
      /*
        This is the function that'll get passed as final argument to each step
        If any arguments get passed to it, they'll replace the initial arguments
      */
      return function (...newArgs) {
        if (typeof steps[index] == 'function') {
          if (newArgs.length) {
            args = newArgs;
          }
          return steps[index](...args, createStepRunner(index + 1));
        } else {
          return steps[index];
        }
      };
    }
  };
}

/**
 * Creates a chain step that will execute the next one
 * only if the request's method matches the provided methodName
 */
function method(methodName) {
  return function (requestInfo, next) {
    if (requestInfo.method == methodName) {
      return next(requestInfo);
    }
  };
}

/**
 * Creates a chain step that will execute the next one
 * only if the request's path matches the provided string or regexp
 *
 * If using a regexp, the match results will be added to the requestInfo
 *
 * @param {string|RegExp} stringOrRegexp
 * @returns
 */
function path(stringOrRegexp) {
  if (typeof stringOrRegexp === 'string') {
    return function (requestInfo, next) {
      if (getPathRelativeToScope(requestInfo.url.pathname) === stringOrRegexp) {
        return next(requestInfo);
      }
    };
  } else {
    return function (requestInfo, next) {
      const matches = getPathRelativeToScope(requestInfo.url.pathname).match(
        stringOrRegexp,
      );
      if (matches) {
        return next({ ...requestInfo, pathParams: matches });
      }
    };
  }
}

let scopePath = '';
function getPathRelativeToScope(fullPath) {
  if (!scopePath) {
    scopePath = new URL(self.registration.scope).pathname;
  }

  return fullPath.replace(scopePath, '/');
}

// # RENDERING
//
// For rendering JavaScript provides pretty much all we need to create
// lightweight templates, by encapsulating template literals (for the actual
// rendering) in functions (for isolation)
function layout(data) {
  const titleText = data.pageTitle
    ? `${data.pageTitle} - ${data.siteTitle}`
    : data.siteTitle;

  return `
  <!doctype html>
  <html>
    <head>
      <title>${titleText}</title>
    </head>
    <body>
      ${data.content}
      <p>
      <small>Made for fun by <a href="https://romaricpascal.is">Romaric Pascal</a> - <a href="https://gitlab.com/romaricpascal/client-side-server">Source code</a></small>
      </p>
    </body>
  </html>
  `;
}

// ## Response helpers
//
// There's a bit of boilerplate for configuring the response options for common
// responses, so we add two helpers to create the right objects more easily:
//
// - one for HTML responses
// - one for redirects
const HTML_HEADERS = {
  'Content-Type': 'text/html',
};

function htmlResponse(content, responseOptions = {}) {
  return {
    body: content,
    responseOptions: {
      ...responseOptions,
      headers: responseOptions.headers
        ? { ...HTML_HEADERS, ...responseOptions.headers }
        : HTML_HEADERS,
    },
  };
}

// ## The actual routes
//
// With all these little helpers in place, we can now finally define the actual routes of our server

const SITE_DATA = {
  siteTitle: 'Client-side multi-page app',
};

const ROUTES = [
  chain(path(/^\/docs/), () => {
    return FORWARD_TO_SERVER;
  }),
  chain(method('GET'), path('/redirect'), () => {
    return Response.redirect('/somewhere-else', 302);
  }),
  chain(method('GET'), path(/\/post-received\/(.*)/), (requestInfo) => {
    const number = requestInfo.pathParams[1];

    return htmlResponse(
      layout({
        ...SITE_DATA,
        pageTitle: 'Post received',
        content: `
<h1>Post received!</h1>
<p>Hurray! You've been redirected here after a form submission.</p>
<p>Hopefully the number you sent through made it OK as well, it's this one, isn't it? ${number}. (it should be in the URL as well)</p>
<p>Accessing it was all thanks to the <code>Request</code> object accessible in the service worker
providing direct access to the <a href="https://developer.mozilla.org/en-US/docs/Web/API/Request/formData">formData</a>,
from which we can then pluck values.</p>
<p>And the redirect as well is provided by the web APIs thanks to a handy <a href="https://developer.mozilla.org/en-US/docs/Web/API/Response/redirect">Response.redirect()</a> function</p>
<p>This marks the end of that little demo. One last link, though. To the annotated source code of the service worker.</p>
<p>I wasn't fully honest saying that <strong>all</strong> requests would end up handled by the service worker. You can still decide to let some go through to the server as you please.</p>
<a href="/client-side-server/docs/service-worker.html">Peek at the Service Worker code</a>
`,
      }),
    );
  }),
  chain(method('POST'), path('/receive-post'), async (requestInfo) => {
    const formData = await requestInfo.request.formData();
    return Response.redirect(
      `/client-side-server/post-received/${formData.get('number')}`,
      302,
    );
  }),
  chain(method('GET'), path(/\/number-in-url\/(.*)/), (requestInfo) => {
    const number = requestInfo.pathParams[1];

    return htmlResponse(
      layout({
        ...SITE_DATA,
        pageTitle: 'Number in the URL',
        content: `
<h1>Number in the URL</h1>
<p>Using regular expressions to match the path, we can extract data from the path itself, rather than carrying parameters in the query string.</p>
<p>Up to you to decide whether to access the params using <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Groups_and_Backreferences#types">regular or named groups</a> in the function that responds.</p>
<p>This is the number you submitted right? ${number}.</p>
<p>Finally, let's try something fancier, submitting a form! And redirecting afterwards, like you'd usually do to avoid issues with reloads.</p>
<form method="post" action="/client-side-server/receive-post">
  <label for="number">Pick a number</label>
  <input id="number" name="number" value="${number}">
  <button>Post the number</button>
</form>
`,
      }),
    );
  }),
  chain(method('GET'), path('/receive-number'), (requestInfo) => {
    const number = requestInfo.url.searchParams.get('number') || 'Nothing';

    return htmlResponse(
      layout({
        ...SITE_DATA,
        pageTitle: 'Receiving a number',
        content: `
<h1>Receiving a number</h1>
<p>That page received: ${number}!</p>
<p>Thanks to <a href="https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams">URLSearchParams</a>,
we can quickly get access to query params and pull the submitted number on the page.</p>
<p>Now let's try something fancier, with a clean URL where the number is inside the path. 
We can't use a form submission for that, so the link below will send to a path with the number you already submitted.</p>
<a href="/client-side-server/number-in-url/${number}">To a page with the number in the URL</a>
`,
      }),
    );
  }),
  chain(method('GET'), path('/simple-navigation'), () => {
    return htmlResponse(
      layout({
        ...SITE_DATA,
        pageTitle: 'After a simple navigation',
        content: `
<h1>After a simple navigation</h1>
<p>Hurray! We can navigate to a new page, still on the client side!</p>
<p>Let's see if we can submit a GET form, to start easy</p>
<form action="/client-side-server/receive-number">
  <label for="number">Pick a number</label>
  <input id="number" name="number">
  <button>Send the number</button>
</form>       
        `,
      }),
    );
  }),
  chain(method('GET'), path('/'), () => {
    return htmlResponse(
      layout({
        ...SITE_DATA,
        pageTitle: 'Welcome to the Client Side Server',
        content: `
<h1>Welcome to the Client Side Server</h1>
<p>If you're reading this, this means the request has been handled by the Service Worker.
No data sent to a server, it all happened on your machine</p>
<a href="/client-side-server/simple-navigation">Let's see another page</a>       
`,
      }),
    );
  }),
  /* This would be better as a "Not found" page, but let's leave it for debugging */
  ({ url }) => {
    return htmlResponse(
      layout({
        ...SITE_DATA,
        pageTitle: url,
        content: getPathRelativeToScope(url.pathname),
      }),
    );
  },
];

self.addEventListener('install', () => {
  console.log('Installing');
  /**
   * @todo Cache resources necessary for the app to work. Not sure if this
   * includes `index.html` and `index.js`. This should definitely include any assets, though
   */
  self.skipWaiting();
  /**
   * @todo: Think of how to handle updates without forcing them
   * onto users when the service worker itself gets automatically
   * updated by the browser
   */
});

// ## Types

/**
 * @callback requestHandler
 * @param {RequestInfo}
 * @returns {ResponseOptions | undefined}
 */

/**
 * @type RequestInfo
 * @property {URL} url - An URL object representing the request's URL
 * @property {string} method - The method of the request (in uppercase)
 * @property {Request} request - The request object, from the FetchEvent
 * @property {FetchEvent} event - The event that triggered the process
 * @property {RegExpExecArray} [pathParams] - Stores any result from matching paths with a regular expression
 */

/**
 * @type {Blob|ArrayBuffer|TypedArray|DataView|FormData|ReadableStream|URLSearchParams|String|string} ResponseBody
 */

/**
 * @type ResponseOptions
 * @property {string|number} [status]
 * @property {string} [statusText]
 * @property {Headers|Object<string,string} [headers]
 */

/**
 * @type ResponseInfo
 * @property {ResponseBody} [body] -- The content to send back to the browser
 * @property {ResponseOptions} [options] -- Status and headers to send alongside the content
 */

(async () => {
  // Service workers may not be supported
  if ('serviceWorker' in navigator) {
    let serviceWorkerReady;
    const output = document.querySelector('output');
    // Prevent navigation until the service worker is ready
    // to avoid not understanding why the reload shows the page again
    document.addEventListener('submit', (event) => {
      if (!serviceWorkerReady) {
        event.preventDefault();
        output.value =
          'The service worker is still being installed and should be available shortly. Please retry in a moment';
        // Clear the output so it makes a new announcement next time the text is set
        setTimeout((output.value = ''), 1000);
      }
    });

    const registration = await navigator.serviceWorker.register(
      '/client-side-server/service-worker.js',
    );

    // We'll obtain the registration as soon as the worker gets registered
    // at that stage, it'll still be installing. We need to wait for it to be installed
    // before moving further as we want the worker to respond to requests
    await hasFinishedInstalling(registration);

    serviceWorkerReady = true;
  } else {
    document
      .querySelector('form')
      .insertAdjacentHTML(
        '<p>Sorry, your browser does not support <a href="https://caniuse.com/serviceworkers">Service Workers</a></p>',
      );
  }
})();

/**
 * Returns a promise resolved with `true` when the given `registration`
 * has finished installing because:
 * - it either has an `active` worker already
 * - or it has no `installing` or `waiting` worker
 * - or the `installing` or `waiting` worker becomes `activated`
 *
 * @param {ServiceWorkerRegistration} registration
 * @returns {true}
 * @throws {Error} If the service worker being installed is made `redundant`
 */
async function hasFinishedInstalling(registration) {
  return new Promise((resolve, reject) => {
    const newServiceWorker = registration.installing || registration.waiting;

    if (registration.active || !newServiceWorker) resolve(true);

    newServiceWorker.addEventListener('statechange', () => {
      if (newServiceWorker.state === 'activated') {
        resolve(true);
      }
      if (newServiceWorker.state === 'redundant') {
        reject(new Error('Service worker was made redundant'));
      }
    });
  });
}

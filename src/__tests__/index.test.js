import { expect } from 'chai';
import sinon from 'sinon';
import Window from 'window';

describe('test setup', function () {
  describe('mocha', function () {
    it('is set up', function () {
      expect(true).to.be.true;
    });
  });
  describe('sinon', function () {
    it('is set up too', function () {
      const spy = sinon.spy();
      spy();
      expect(spy).to.have.been.called;
    });
  });
  describe('window', function () {
    it('is also set up', function () {
      const { document } = new Window();
      const a = document.createElement('a');
      a.setAttribute('href', 'http://example.com');
      expect(a.href).to.equal('http://example.com/');
    });
  });
});
